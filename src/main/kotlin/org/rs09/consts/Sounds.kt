package org.rs09.consts

object Sounds {

    const val OPEN_CABINET_44 = 44
    const val OPEN_HEAVY_54 = 54
    const val WATER_BEING_POURED_2982 = 2982
    const val KEY_DROPPING_DOWN_DRAIN_2983 = 2983
    const val TRAIBORN_SUMMON_WARDROBE_2973 = 2973
    const val RECEIVE_SILVERLIGHT_2990 = 2990

}